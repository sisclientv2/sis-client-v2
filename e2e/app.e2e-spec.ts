import { SisClientPage } from './app.po';

describe('sis-client App', () => {
  let page: SisClientPage;

  beforeEach(() => {
    page = new SisClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
