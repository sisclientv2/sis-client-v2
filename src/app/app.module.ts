import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DashboardModule} from './dashboard/dashboard.module';
import {ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2MobxModule } from 'ng2-mobx';
import { AppComponent } from './app.component';
import { routes } from './app.routes';
import {StudentService} from './core/services/domain/student.service';
import {CommonService} from './core/services/common/common.service';
import {ParentService} from './core/services/domain/parent.service';
import {SubjectService} from './core/services/domain/subject.service';
import {GradeService} from './core/services/domain/grade.service';
import {ExamService} from './core/services/domain/exam.service';
import {MarkService} from './core/services/domain/mark.service';
import {DummyDataService} from './core/services/common/dummy-data.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    Ng2MobxModule,
    
    RouterModule.forRoot(routes),
    FormsModule,
    HttpModule,
    DashboardModule
  ],
  providers: [
    StudentService,
    CommonService,
    ParentService,
    SubjectService,
    GradeService,
    ExamService,
    MarkService,
    DummyDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
