export class Address {
    public address1: string;
    public address2: string;
    public city: string;
    public state: string;
    public country: string;
    public pin: number;

    constructor(object?: any){
        this.address1 = object && object.address1 || null;
        this.address2 = object && object.address2 || null;
        this.city = object && object.city || null;
        this.state = object && object.state || null;
        this.country = object && object.country || null;
        this.pin = object && object.pin || null;
    }
}