export class Car {
    code: string;
    description: string;

    constructor(object?:any){
        this.code = object && object.code || null;
        this.description = object && object.description || null;
    }
}