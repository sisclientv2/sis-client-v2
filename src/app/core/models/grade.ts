import {Subject} from './subject';
export class Grade{
    code: string;
    description: string;
    subjects: Subject;

    constructor(code?: string, description?: string, subjects?: Subject){
        this.code = code || null;
        this.description = description || null;
    }
}