import {Exam} from './exam';
import {Subject} from './subject';

export class Mark{
    marks: number;
    abscent: boolean;
    exam: Exam;
    subject: Subject;
    studentName: string;
    constructor(object?: any){
        this.studentName = object && object.studentName || null;
        this.marks = object && object.marks || null;
        this.abscent = object && object.abscent || null;
        if(object && object.exam){
            this.exam = new Exam(object.exam);
        }
        if(object && object.subject){
            this.subject = new Subject(object.subject);
        }
    }
}