import {Address} from "./address";

export class Parent{
    public userId: string;
    public userType: string;
    public firstName: string;
    public lastName: string;
    public address: Address;

    constructor(object?:any){
        this.userId = object && object.userId || null;
        this.userType = object && object.userType || null;
        this.firstName = object && object.firstName || null;
        this.lastName = object && object.lastName || null;
        if(object && object.address){
            this.address = new Address(object.address);
        }
    }

}