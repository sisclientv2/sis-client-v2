export class SortParam {
    order: string;
    
    constructor(order: string){
        this.order = order || null;
    }
}