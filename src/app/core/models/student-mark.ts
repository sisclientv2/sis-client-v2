import {Subject} from './subject';
export class StudentMark{
    public semester: any;
    public subject: Subject;
    public mark: string;
    constructor(semester?: any, subject?: Subject, mark?: string ){
        this.semester = semester || null;
        this.subject = subject || null;
        this.mark = mark || null;
    }
}