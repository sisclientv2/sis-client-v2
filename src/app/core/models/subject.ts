export class Subject{
    code: string;
    description: string;
    constructor(code?: string, description?: string){
        this.code = code || null;
        this.description = description || null;
    }
}
