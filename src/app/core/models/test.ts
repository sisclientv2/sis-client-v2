import {Exam} from './exam';
import {Subject} from './subject';

export class Test {
    name: string;
    city: string;
    exam: Exam;
    subject: Subject;
    constructor(object?: any){
        this.name = object && object.name || null;
        this.city = object && object.city || null;
        if(object && object.exam){
            this.exam = new Exam(object.exam);
        }
        if(object && object.subject){
            this.subject = new Subject(object.subject);
        }
    }
}