import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CommonService {

  constructor(private http: Http) { }


  getUserTypes() {
    var userTypes = [
          {label:'Select User Type',value:null},
          {label:'Student',value:'Student'},
          {label:'Parent',value:'Parent'},
          {label:'TeachingStaff',value:'TeachingStaff'},
          {label:'NonTeachingStaff',value:'NonTeachingStaff'}
        ];
    return userTypes;
  }

  getAllSemesters(){
    var semesters = [
      {label:'Select Semester', value:null},
      {label:'sem1', value: 'sem1'},
      {label:'semester2', value: 'semester2'},
      {label:'semester3', value: 'semester3'},
      {label:'semester4', value: 'semester4'},
      {label:'semester5', value: 'semester5'}
    ];
    return semesters;
  }

  getGenders() {
    var genders = [
          {label:'Select Gender',value:null},
          {label:'Male',value:'Male'},
          {label:'Female',value:'Female'}
        ];
    return genders;
  }

  getGrades() {
    var grades = [
          {label:'Select Grade',value:null},
          {label:'Grade1',value:'Grade1'},
          {label:'Grade2',value:'Grade2'},
          {label:'Grade3',value:'Grade3'},
          {label:'Grade4',value:'Grade4'}
        ];
    return grades;
  }


  getUserDetail(userId){
    return this.http.get("http://localhost:8080/parents/" + userId)
      .map(res => res.json());
  }

  getUser(userId){
    return this.http.get("http://localhost:8080/users/" + userId)
      .map(res => res.json());
  }

  getAllUsers(userType){
    return this.http.get("http://localhost:8080/users/"+userType+"/images")
      .map(res => res.json());
  }

  createUser(user){
    return this.http.post("http://localhost:8080/users", user)
      .map((response: Response) => {
        return response;
      });

  }

}
