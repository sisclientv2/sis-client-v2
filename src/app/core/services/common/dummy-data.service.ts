import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DummyDataService {

  constructor(private http: Http) { }

  getAllMarks() {
    let marks = [10, 20, 30, 40, 50];
  }

  getAllSubj() {
    let subjects = ["sub1", "sub2", "sub3", "sub4", "sub5"];
  }
  
  getTestData() {
    let test = [
      {
        name: "john",
        city: "CA",
        subject: {
          code: "subj1",
          description: "subj1"
        },
        exam: {
          code: "month1",
          description: "month1"
        }
      },
      {
        name: "jack",
        city: "SE",
        subject: {
          code: "subj2",
          description: "subj2"
        },
        exam: {
          code: "month2",
          description: "month2"
        }
      },
      {
        name: 'peter',
        city: 'QA',
        exam: {
          code: 'exam3',
          description: 'exam3'
        },
        subject: {
          code: 'subj3',
          description: 'subj3'
        }
      }
    ];

    return test;
  }

}
