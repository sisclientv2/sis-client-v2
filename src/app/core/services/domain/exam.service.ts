import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ExamService {

  constructor(private _http: Http) { }

  getAllExams(){
    return this._http.get("http://localhost:8080/exams")
      .map(res => res.json());
  }

  createExam(exam){
    return this._http.post("http://localhost:8080/exams", exam)
      .map((response: Response) => {
            return response;
        });
  }

  updateExam(exam){
    return this._http.put("http://localhost:8080/exams/"+exam.code, exam)
      .map((res: Response) => {
        return res;
      });
  }

  deleteExam(code){
    return this._http.delete("http://localhost:8080/exams/" + code)
      .map((response: Response) => {
        return response;
      });
  }

}
