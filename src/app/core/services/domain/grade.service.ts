import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GradeService {
    
  constructor(private _http: Http) { }

    getAllGrades() {
        return this._http.get("http://localhost:8080/grades")
        .map(res => res.json());
    }

    createGrade(grade) {
        return this._http.post("http://localhost:8080/grades", grade)
            .map((response: Response) => {
                return response;
            });
    }

    deleteGrade(code) {
        return this._http.delete("http://localhost:8080/grades/" + code)
            .map((response: Response) => {
                return response;
            });
    }

    updateGrade(grade) {
        return this._http.put("http://localhost:8080/grades/"+grade.code, grade)
            .map((response: Response) => {
                return response;
            });
    }




}
