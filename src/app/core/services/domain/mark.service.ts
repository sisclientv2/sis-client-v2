import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MarkService {

  constructor(private http: Http) { }

  getAllMarks(){
    return this.http.get("http://localhost:8080/students")
      .map(res => res.json());
  }

  createMarks(mark){
    return this.http.post("http://localhost:8080/students/marks", mark)
      .map((response: Response) => {
        return response;
      });
  }

}
