import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ParentService {

  constructor(private http: Http) { }

  getParents(){
    return this.http.get("http://localhost:8080/users/parents/images")
      .map(res => res.json());
  }

  createParents(parent){
   return this.http.post("http://localhost:8080/parents", parent)
      .map((response: Response) => {
        return response;
      });
  }

}
