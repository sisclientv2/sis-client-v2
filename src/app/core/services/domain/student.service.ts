import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class StudentService {

  constructor(private _http: Http) { 
  }

  getAllStudents(){
    return this._http.get("http://localhost:8080/students")
      .map(res => res.json());
  }

  getStudent(userId){
      return this._http.get("http://localhost:8080/students/" + userId)
        .map(res => res.json());
  }
  
  createStudents(student) {
      return this._http.post("http://localhost:8080/students", student)
          .map((response: Response) => {
              return response;
          });
  }

}
