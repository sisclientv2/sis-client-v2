import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SubjectService {
    
  constructor(private _http: Http) { }

  getAllSubjects() {

      return this._http.get("http://localhost:8080/subjects")
      .map(res => res.json());
    }

  createSubject(subject) {
      return this._http.post("http://localhost:8080/subjects", subject)
          .map((response: Response) => {
              return response;
          });
  }

  deleteSubject(code) {
      return this._http.delete("http://localhost:8080/subjects/" + code)
          .map((response: Response) => {
              return response;
          });
  }

  updateSubject(subject) {
      return this._http.put("http://localhost:8080/subjects/"+subject.code, subject)
          .map((response: Response) => {
              return response;
          });
  }

}
