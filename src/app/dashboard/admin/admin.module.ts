import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AdminComponent} from './admin.component';
import {StudentManagementModule} from './student-management/student-management.module';
import {StaffManagementModule} from './staff-management/staff-management.module';
import {ParentManagementModule} from './parent-management/parent-management.module';
import {RouterModule} from "@angular/router";
import { HomeComponent } from './home/home.component';
import {SubjectManagementModule} from './subject-management/subject-management.module';
import {GradeManagementModule} from './grade-management/grade-management.module';
import {ExamManagementModule} from './exam-management/exam-management.module';
import { TestManagementModule } from './test-management/test-management.module';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    StudentManagementModule,
    StaffManagementModule,
    ParentManagementModule,
    SubjectManagementModule,
    GradeManagementModule,
    ExamManagementModule,
    TestManagementModule
  ],
  declarations: [AdminComponent, HomeComponent],
  exports: [AdminComponent]
})
export class AdminModule { }
