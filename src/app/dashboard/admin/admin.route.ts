import {Route} from '@angular/router';

import {HomeRoute} from './home/home.route';
import {StudentManagementRoutes} from './student-management/student-management.route';
import {ParentManagementRoutes} from './parent-management/parent-management.route';
import {StaffManagementRoutes} from './staff-management/staff-management.route';
import {SubjectManagementRoutes} from './subject-management/subject-management.route';
import {GradeManagementRoutes} from './grade-management/grade-management.route';
import {ExamManagementRoutes} from './exam-management/exam-management.route';
import {TestManagementRoutes} from './test-management/test-management.route';

export const AdminRoutes: Route[] = [
    ...HomeRoute,
    ...StudentManagementRoutes,
    ...ParentManagementRoutes,
    ...StaffManagementRoutes,
    ...SubjectManagementRoutes,
    ...GradeManagementRoutes,
    ...ExamManagementRoutes,
    ...TestManagementRoutes
];