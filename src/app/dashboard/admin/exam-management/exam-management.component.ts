import { Component, OnInit } from '@angular/core';
import {ExamService} from './../../../core/services/domain/exam.service';
import {Exam} from './../../../core/models/exam';

@Component({
  selector: 'app-exam-management',
  templateUrl: './exam-management.component.html',
  styleUrls: ['./exam-management.component.css']
})
export class ExamManagementComponent implements OnInit {

  exams: Exam;
  exam: Exam;
  newExam: boolean;
  displayDialog: boolean;

  constructor(private examService: ExamService) { }

  ngOnInit() {
    this.getAllExams();
  }

  getAllExams(){
    this.examService.getAllExams()
      .subscribe(exams => {
        this.exams = exams;
      });
  }

  showDialogToAdd(){
    this.newExam = true;
    this.displayDialog = true;
    this.exam = new Exam();
  }

  save(){
    if(this.newExam){
        this.examService.createExam(this.exam)
        .subscribe(() => {
          console.log("Exams created Successfully");
          this.getAllExams();
        }, error => {
          console.log("Error adding in exams");
        });
      
    }else if(!this.newExam){
      this.examService.updateExam(this.exam)
        .subscribe(() => {
          console.log("Exam Updated success");
          this.getAllExams();
        },error => {
          console.log("Error in updating exams");
        });
    }

    this.exam = null;
    this.displayDialog = false;
    

  }

  delete(){
    this.examService.deleteExam(this.exam.code)
      .subscribe(() => {
        console.log("Exam deleted successfully");
        this.getAllExams();
      }, error => {
        console.log("Error deleting in exam");
      });
    this.exam = null;
    this.displayDialog = false;
  }

  onRowSelect(event){
    this.newExam = false;
    this.exam = this.cloneExam(event.data);
    this.displayDialog = true;
  }

  cloneExam(c: Exam): Exam {
        let exam = new Exam();
        for(let prop in c) {
            exam[prop] = c[prop];
        }
        return exam;
    }

}
