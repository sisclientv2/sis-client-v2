import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ExamManagementComponent} from './exam-management.component';
import {InputTextModule,DataTableModule,ButtonModule,DialogModule} from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ExamManagementComponent]
})
export class ExamManagementModule { }
