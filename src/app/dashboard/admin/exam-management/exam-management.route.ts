import {Route} from '@angular/router';
import {ExamManagementComponent} from './exam-management.component';

export const ExamManagementRoutes: Route[] = [
    {
        path: 'exams',
        component: ExamManagementComponent
    }
];