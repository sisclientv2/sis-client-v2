import { Component, OnInit } from '@angular/core';
import { Grade } from './../../../../core/models/grade';
import { GradeService } from './../../../../core/services/domain/grade.service';
import { SubjectService } from './../../../../core/services/domain/subject.service';
import { Subject } from './../../../../core/models/subject';

@Component({
  selector: 'app-assign-subject',
  templateUrl: './assign-subject.component.html',
  styleUrls: ['./assign-subject.component.css']
})
export class AssignSubjectComponent implements OnInit {

  grades;
  selectedGrade;
  subjects;

  sourceSubjects;
  selectedSubjects;
  constructor(private gradeService: GradeService, private subjectService: SubjectService) {
  }

  ngOnInit() {
    this.getAllGrades();
  }

  getAllGrades() {
    this.gradeService.getAllGrades().subscribe(grades => {
      this.grades = grades;
    })
  }

  getAllSubjects(gradeSelect) {
    this.subjectService.getAllSubjects()
      .subscribe(subjects => {
        this.sourceSubjects = subjects;
        this.selectedSubjects = gradeSelect.data.subjects;
        if(this.selectedSubjects){
          for (let i = 0; i < this.selectedSubjects.length; i++) {
            let selectedSubj = this.selectedSubjects[i].code;
            for (let j = 0; j < this.sourceSubjects.length; j++) {
              let sourceSubj = this.sourceSubjects[j].code;
              if (sourceSubj == selectedSubj) {
                this.sourceSubjects.splice(j, 1);
              }
            }
          }
        }else{
          this.selectedSubjects = [];
        }
      });
  }

  onRowSelect(gradeSelect) {
    if (gradeSelect.data) {
      this.getAllSubjects(gradeSelect);
    }
  }

  saveSelectedSubjects() {
    if (this.selectedGrade) {
      this.selectedGrade.subjects = this.selectedSubjects;
      this.gradeService.createGrade(this.selectedGrade).subscribe(() => {
        console.info("Subjects Assigned to Grade Successfully");
      },
        error => {
          console.info("Error Assigning in Subjects to Grade");
        });
    }
  }

}
