import { Component, OnInit } from '@angular/core';
import {Grade} from './../../../../core/models/grade';
import {GradeService} from './../../../../core/services/domain/grade.service';

@Component({
  selector: 'app-grade-create',
  templateUrl: './grade-create.component.html',
  styleUrls: ['./grade-create.component.css']
})
export class GradeCreateComponent implements OnInit {
  displayDialog: boolean;

    grade: Grade;
    newGrade: boolean;
    grades: Grade;

  constructor(private gradeService: GradeService) {
   }

  ngOnInit() {
    this.getAllGrades();
  }

  getAllGrades(){
    this.gradeService.getAllGrades().subscribe(grades => {
      this.grades = grades;
    })
  }

  showDialogToAdd() {
      this.newGrade = true;
      this.grade = new Grade();
      this.displayDialog = true;
  }

  save() {
        if(this.newGrade){
            this.gradeService.createGrade(this.grade).subscribe(() => {
                console.info("Grade Added Successfully");
                this.getAllGrades();
            },
            error => {
                console.info("Error Adding in Grade Data");
            });
        }else if(!this.newGrade){
            this.gradeService.updateGrade(this.grade).subscribe(() => {
                console.info("Grade Updated Successfully");
                this.getAllGrades();
            },
            error => {
                console.info("Error Updating in Grade Data");
            });
        }

        this.grade = null;
        this.displayDialog = false;
    }

    delete() {
        this.gradeService.deleteGrade(this.grade.code).subscribe(() => {
            console.info("Grade Deleted Successfully");
            this.getAllGrades();
        },
        error => {
            console.info("Error Deleting in Grade Data");
        });

        this.grade = null;
        this.displayDialog = false;
    }

    onRowSelect(event) {
        this.newGrade = false;
        this.grade = this.cloneGrade(event.data);
        this.displayDialog = true;
    }
    
    cloneGrade(c: Grade): Grade {
        let grade = new Grade();
        for(let prop in c) {
            grade[prop] = c[prop];
        }
        return grade;
    }

}
