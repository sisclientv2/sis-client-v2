import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradeManagementComponent } from './grade-management.component';
import {InputTextModule,DataTableModule,ButtonModule,DialogModule} from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import { AssignSubjectComponent } from './assign-subject/assign-subject.component';
import { GradeCreateComponent } from './grade-create/grade-create.component';
import {PickListModule, ConfirmDialogModule, SharedModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    PickListModule,
    ConfirmDialogModule,
    SharedModule
  ],
  declarations: [GradeManagementComponent, AssignSubjectComponent, GradeCreateComponent],
  exports: [GradeManagementComponent]
})
export class GradeManagementModule { }
