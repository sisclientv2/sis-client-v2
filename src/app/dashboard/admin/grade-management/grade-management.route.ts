import {Route} from '@angular/router';
import {GradeManagementComponent} from './grade-management.component';
import {AssignSubjectComponent} from './assign-subject/assign-subject.component';
import {GradeCreateComponent} from './grade-create/grade-create.component';

export const GradeManagementRoutes: Route[] = [
    {
        path: 'grades',
        component: GradeManagementComponent,
        children: [
            {path: 'assign-subject', component: AssignSubjectComponent},
            {path: 'grade-create', component: GradeCreateComponent}
        ]
    }
];