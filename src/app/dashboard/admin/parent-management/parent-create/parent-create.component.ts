import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, FormBuilder, Validators} from '@angular/forms';
import {ParentService} from './../../../../core/services/domain/parent.service';
import {CommonService} from './../../../../core/services/common/common.service';


@Component({
  selector: 'app-parent-create',
  templateUrl: './parent-create.component.html',
  styleUrls: ['./parent-create.component.css']
})
export class ParentCreateComponent implements OnInit {

  private createParentForm: FormGroup;

  userTypes;
  genders;

  constructor(private fb: FormBuilder,
              private parentService: ParentService,
              private commonService: CommonService
              )
 { 
   this.userTypes = this.commonService.getUserTypes();
   this.genders = this.commonService.getGenders();

  }

  ngOnInit() {
    this.createParentForm = this.fb.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      userId: new FormControl(),
      userType: new FormControl(),
      gender: new FormControl(),
      birthDate: new FormControl(),
      address: new FormGroup({
        address1: new FormControl(),
        address2: new FormControl(),
        city: new FormControl(),
        state: new FormControl(),
        pin: new FormControl(),
        country: new FormControl()
      }),

    });

  }

  saveParentData(value){
    this.parentService.createParents(value)
      .subscribe( () => {
        console.info("Parent Added Successfully");
        this.createParentForm.reset();
      },
      error => {
        console.info("Error Adding in Parent Data");
      });
  }


}
