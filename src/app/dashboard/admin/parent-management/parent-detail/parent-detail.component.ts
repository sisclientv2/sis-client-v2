import { Component, OnInit } from '@angular/core';
import {CommonService} from '../../../../core/services/common/common.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-parent-detail',
  templateUrl: './parent-detail.component.html',
  styleUrls: ['./parent-detail.component.css']
})
export class ParentDetailComponent implements OnInit {

  userId;
  parentData;

  constructor(private commonService: CommonService, private activatedRoute: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.activatedRoute.params
      .map(params => params['userId'])
      .subscribe((userId) => {
        this.commonService.getUserDetail(userId)
          .subscribe(parent => {
            this.parentData = parent;
          })
      })
  }

  backPage(){
    this.location.back();
  }

}
