import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {observable, computed, action} from 'mobx';
import {UIStore} from "../store/ui-store";
import {Store} from "../store/store";

@Component({
  selector: 'app-parent-list',
  templateUrl: './parent-list.component.html',
  styleUrls: ['./parent-list.component.css']
})
export class ParentListComponent implements OnInit {

  parents: any[];

  @observable searchName: string = null;

  @action setSearchName(searchName: string) {
        this.searchName = searchName;
    }

  constructor(
              private router:Router,
              private uiStore: UIStore,
              private store: Store) {
  }

  ngOnInit() {
    this.refresh();
  }

  refresh() {
      this.store.loadParents();
  }

  viewStudentDetail(userId){
    this.router.navigate(['parents/parentsDetail/', userId])
  }
}
