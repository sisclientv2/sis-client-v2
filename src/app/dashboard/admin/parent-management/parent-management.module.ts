import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ParentManagementComponent} from './parent-management.component';
import { ParentCreateComponent } from './parent-create/parent-create.component';
import { ParentListComponent } from './parent-list/parent-list.component';
import {RouterModule} from "@angular/router";
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import { ParentDetailComponent } from './parent-detail/parent-detail.component';
import { SearchPipe } from './pipes/search.pipe';
import { SortByPipe } from './pipes/sort-by.pipe';
import {SearchBarModule} from './../shared/modules/search-bar/search-bar.module';
import {UIStore} from './store/ui-store';
import {Store} from './store/store';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    FileUploadModule,
    SearchBarModule
  ],
  declarations: [
    ParentManagementComponent,
    ParentCreateComponent,
    ParentListComponent,
    ParentDetailComponent,
    SearchPipe,
    SortByPipe
  ],
  providers: [
    UIStore,
    Store
  ]
})
export class ParentManagementModule { }
