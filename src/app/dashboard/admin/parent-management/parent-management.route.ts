import {Route} from '@angular/router';
import {ParentManagementComponent} from './parent-management.component';
import {ParentListComponent} from './parent-list/parent-list.component';
import {ParentCreateComponent} from './parent-create/parent-create.component';
import {ParentDetailComponent} from './parent-detail/parent-detail.component';

export const ParentManagementRoutes: Route[] = [
    {
        path: 'parents',
        component: ParentManagementComponent,
        children: [
            {path: 'parentsList', component: ParentListComponent},
            {path: 'parentsCreate', component: ParentCreateComponent},
            {path: 'parentsDetail/:userId', component: ParentDetailComponent}
        ]
    }
];