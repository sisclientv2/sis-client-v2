import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchPipe'
})
export class SearchPipe implements PipeTransform {

  transform(data: any[], searchTerm: string): any[] {
    if(!!searchTerm){
      searchTerm = searchTerm.toUpperCase();
      return data.filter(item => {
        return item.userId.toUpperCase().indexOf(searchTerm) !== -1 
      });
    }
    return data;
  }

}
