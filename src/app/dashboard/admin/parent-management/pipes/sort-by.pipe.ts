import { Pipe, PipeTransform } from '@angular/core';
import {SortParam} from "../../../../core/models/sort-param";

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {
  transform(data: any[], sortParam: SortParam): any[] {
    if(data){
       return data.sort((a: any, b: any) => {
            if (a.userId < b.userId) {
                return (sortParam.order === "asc" ) ? -1 : 1;
            } else if (a.userId > b.userId) {
                return (sortParam.order === "asc") ? 1 : -1;
            } else {
                return 0;
            }
        });
    }
    return data;
  }

}
