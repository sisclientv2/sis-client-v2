import {observable, action, computed} from 'mobx';
import {Parent} from "../../../../core/models/parent";
import {ParentService} from '../../../../core/services/domain/parent.service';
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";

@Injectable()
export class Store {
    @observable parents: Parent[] = [];

    constructor(private parentService: ParentService){

    }

    @action loadParents() {
        this.parentService.getParents()
        .subscribe(parents =>{
            this.parents = parents;
        });
    }


}