import {observable, computed, action} from 'mobx';
import {SortParam} from "../../../../core/models/sort-param";
import {Injectable} from "@angular/core";

@Injectable()
export class UIStore {
    @observable sortParam: SortParam = new SortParam('asc');

    @action setSortParam(order: string) {
        this.sortParam = new SortParam(order);
    }
}