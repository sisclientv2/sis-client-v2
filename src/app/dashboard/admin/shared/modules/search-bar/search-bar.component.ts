import { Component, ViewChild, ElementRef, OnInit, EventEmitter, Input, Output } from '@angular/core';
import {Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output() searchChangeEmitter: EventEmitter<string> = new EventEmitter<string>();
  @Output() nameAscending: EventEmitter<any> = new EventEmitter<string>();
  @Output() nameDescending: EventEmitter<any> = new EventEmitter<string>();

  private searchUpdated: Subject<string> = new Subject<string>();

  private isAscendingOrder: boolean = true;

  constructor() {
    this.searchChangeEmitter = <any>this.searchUpdated.asObservable();
   }

  ngOnInit() {
  }

  onSearchTyped(value: string) {
      this.searchUpdated.next(value);
  }


    onNameAscending() {
        this.isAscendingOrder = true;
        this.nameAscending.next();
    }

    onNameDescending() {
        this.isAscendingOrder = false;
        this.nameDescending.next();
    }
}
