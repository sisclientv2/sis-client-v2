import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonTeachingStaffCreateComponent } from './non-teaching-staff-create.component';

describe('NonTeachingStaffCreateComponent', () => {
  let component: NonTeachingStaffCreateComponent;
  let fixture: ComponentFixture<NonTeachingStaffCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonTeachingStaffCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTeachingStaffCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
