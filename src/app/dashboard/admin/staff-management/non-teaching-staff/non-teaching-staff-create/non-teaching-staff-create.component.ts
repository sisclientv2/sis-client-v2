import { Component, OnInit } from '@angular/core';
import {CommonService} from './../../../../../core/services/common/common.service';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';

@Component({
  selector: 'app-non-teaching-staff-create',
  templateUrl: './non-teaching-staff-create.component.html',
  styleUrls: ['./non-teaching-staff-create.component.css']
})
export class NonTeachingStaffCreateComponent implements OnInit {
  createNonTeachingStaffForm;
  userType;
  genders;

  constructor(private commonService: CommonService, private fb: FormBuilder) { }

  ngOnInit() {

    // get all the common data from the common service
    this.userType = this.commonService.getUserTypes();
    this.genders = this.commonService.getGenders();


    this.createNonTeachingStaffForm = this.fb.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      userId: new FormControl(),
      userType: new FormControl(),
      gender: new FormControl(),
      birthDate: new FormControl(),
      address: new FormGroup({
        address1: new FormControl(),
        address2: new FormControl(),
        city: new FormControl(),
        state: new FormControl(),
        country: new FormControl(),
        pin: new FormControl()
      })
    });

  }

  createNonTeachingStaffData(value){
    this.commonService.createUser(value).subscribe(() => {
      console.log("Non TeachingStaff added successfully");
      this.createNonTeachingStaffForm.reset();
    },
    error => {
      console.log("Error in adding Non Teaching Staff");
    });
  }

}
