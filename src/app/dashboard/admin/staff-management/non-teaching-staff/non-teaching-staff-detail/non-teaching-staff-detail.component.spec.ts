import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonTeachingStaffDetailComponent } from './non-teaching-staff-detail.component';

describe('NonTeachingStaffDetailComponent', () => {
  let component: NonTeachingStaffDetailComponent;
  let fixture: ComponentFixture<NonTeachingStaffDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonTeachingStaffDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTeachingStaffDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
