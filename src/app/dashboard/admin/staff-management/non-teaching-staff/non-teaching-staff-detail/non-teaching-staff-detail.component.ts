import { Component, OnInit } from '@angular/core';
import {CommonService} from './../../../../../core/services/common/common.service';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-non-teaching-staff-detail',
  templateUrl: './non-teaching-staff-detail.component.html',
  styleUrls: ['./non-teaching-staff-detail.component.css']
})
export class NonTeachingStaffDetailComponent implements OnInit {
  nonTeachingStaffData;

  constructor(private commonService: CommonService, 
              private activatedRoute: ActivatedRoute,
              private location: Location) { }

  ngOnInit() {

    // get user data from the common service
    this.activatedRoute.params
      .map(params => params['userId'])
      .subscribe((userId) => {
        this.commonService.getUser(userId)
          .subscribe(nonTeachingStaff => {
            this.nonTeachingStaffData = nonTeachingStaff;
          })
      });


  }

  backPage(){
    this.location.back();
  }

}
