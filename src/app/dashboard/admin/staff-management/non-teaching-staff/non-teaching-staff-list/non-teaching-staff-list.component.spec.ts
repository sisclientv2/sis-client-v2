import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonTeachingStaffListComponent } from './non-teaching-staff-list.component';

describe('NonTeachingStaffListComponent', () => {
  let component: NonTeachingStaffListComponent;
  let fixture: ComponentFixture<NonTeachingStaffListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonTeachingStaffListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonTeachingStaffListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
