import { Component, OnInit } from '@angular/core';
import {CommonService} from './../../../../../core/services/common/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-non-teaching-staff-list',
  templateUrl: './non-teaching-staff-list.component.html',
  styleUrls: ['./non-teaching-staff-list.component.css']
})
export class NonTeachingStaffListComponent implements OnInit {
  nonTeachingStaffData;
  searchValue;
  sortValue = 'asc';

  constructor(private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    // get the non-teaching staff data from the service
    this.commonService.getAllUsers("NonTeachingStaff")
      .subscribe(nonTeachingStaff => {
        this.nonTeachingStaffData = nonTeachingStaff;
      })
  }

  getUserData(value){
    this.router.navigate(['staffs/nonTeachingStaff/nonTeachingStaffDetail', value]);
  }

  setSearchName(searchValue){
    this.searchValue = searchValue;
  }

  setSorting(sortValue){
    this.sortValue = sortValue;
  }

}
