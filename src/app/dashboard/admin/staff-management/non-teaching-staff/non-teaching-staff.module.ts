import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NonTeachingStaffListComponent } from './non-teaching-staff-list/non-teaching-staff-list.component';
import { NonTeachingStaffCreateComponent } from './non-teaching-staff-create/non-teaching-staff-create.component';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import { NonTeachingStaffDetailComponent } from './non-teaching-staff-detail/non-teaching-staff-detail.component';
import {RouterModule} from "@angular/router";
import {NonTeachingStaffComponent} from './non-teaching-staff.component';
import {SearchBarModule} from './../../shared/modules/search-bar/search-bar.module';
import { SortByPipe } from './pipes/sort-by.pipe';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    RouterModule,
    SearchBarModule
  ],
  declarations: [
    NonTeachingStaffComponent,
    NonTeachingStaffListComponent, 
    NonTeachingStaffCreateComponent, 
    NonTeachingStaffDetailComponent,
    SortByPipe,
    SearchPipe
  ],
  exports: [NonTeachingStaffComponent]
})
export class NonTeachingStaffModule { }
