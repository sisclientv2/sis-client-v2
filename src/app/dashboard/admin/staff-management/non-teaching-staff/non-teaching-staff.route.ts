import {Route} from '@angular/router';
import {NonTeachingStaffComponent} from './non-teaching-staff.component';
import {NonTeachingStaffListComponent} from './non-teaching-staff-list/non-teaching-staff-list.component';
import {NonTeachingStaffCreateComponent} from './non-teaching-staff-create/non-teaching-staff-create.component';
import {NonTeachingStaffDetailComponent} from './non-teaching-staff-detail/non-teaching-staff-detail.component';

export const NonTeachingStaffRoutes: Route[] = [
    {
        path: 'nonTeachingStaff',
        component: NonTeachingStaffComponent,
        children: [
            {path: 'nonTeachingStaffList', component: NonTeachingStaffListComponent},
            {path: 'nonTeachingStaffCreate', component: NonTeachingStaffCreateComponent},
            {path: 'nonTeachingStaffDetail/:userId', component: NonTeachingStaffDetailComponent}
        ]
    }
];