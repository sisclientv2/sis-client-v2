import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TeachingStaffModule} from './teaching-staff/teaching-staff.module';
import {NonTeachingStaffModule} from './non-teaching-staff/non-teaching-staff.module';
import {StaffManagementComponent} from './staff-management.component';
import {RouterModule} from '@angular/router';
import { NonTeachingStaffComponent } from './non-teaching-staff/non-teaching-staff.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TeachingStaffModule,
    NonTeachingStaffModule
  ],
  declarations: [
    StaffManagementComponent
  ]
})
export class StaffManagementModule { }
