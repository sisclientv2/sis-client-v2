import {Route} from '@angular/router';
import {StaffManagementComponent} from './staff-management.component';
import {TeachingStaffComponent} from './teaching-staff/teaching-staff.component';
import {NonTeachingStaffComponent} from './non-teaching-staff/non-teaching-staff.component';
import {TeachingStaffRoutes} from './teaching-staff/teaching-staff.route';
import {NonTeachingStaffRoutes} from './non-teaching-staff/non-teaching-staff.route';

export const StaffManagementRoutes: Route[] = [
    {
        path: 'staffs',
        component: StaffManagementComponent,
        children: [
            ...TeachingStaffRoutes,
            ...NonTeachingStaffRoutes
        ]
    }
];