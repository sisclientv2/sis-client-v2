import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingStaffCreateComponent } from './teaching-staff-create.component';

describe('TeachingStaffCreateComponent', () => {
  let component: TeachingStaffCreateComponent;
  let fixture: ComponentFixture<TeachingStaffCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingStaffCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingStaffCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
