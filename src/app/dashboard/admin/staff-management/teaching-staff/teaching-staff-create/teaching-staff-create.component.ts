import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {CommonService} from './../../../../../core/services/common/common.service';
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';



@Component({
  selector: 'app-teaching-staff-create',
  templateUrl: './teaching-staff-create.component.html',
  styleUrls: ['./teaching-staff-create.component.css']
})
export class TeachingStaffCreateComponent implements OnInit {

  private createTeachingStaffForm: FormGroup;
  genders;
  userType;

  constructor(private commonService: CommonService,
              private fb: FormBuilder
              ) { }

  ngOnInit() {
    // get the common data from common service
    this.genders = this.commonService.getGenders();
    this.userType = this.commonService.getUserTypes();



    this.createTeachingStaffForm = this.fb.group({
      firstName: new FormControl(),
      lastName: new FormControl(),
      userType: new FormControl(),
      userId: new FormControl(),
      gender: new FormControl(),
      birthDate: new FormControl(),
      address: new FormGroup({
        address1: new FormControl(),
        address2: new FormControl(),
        city: new FormControl(),
        state: new FormControl(),
        country: new FormControl(),
        pin: new FormControl()
      })
    })

  }

  saveTeachingStaff(value){
    this.commonService.createUser(value).subscribe(() => {
      console.log("Teaching Staff Added successfully");
      this.createTeachingStaffForm.reset();
    }, 
    error => {
      console.log("Error adding in Teaching staff");
    });
  }

}
