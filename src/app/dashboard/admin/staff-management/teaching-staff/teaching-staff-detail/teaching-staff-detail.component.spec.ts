import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingStaffDetailComponent } from './teaching-staff-detail.component';

describe('TeachingStaffDetailComponent', () => {
  let component: TeachingStaffDetailComponent;
  let fixture: ComponentFixture<TeachingStaffDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingStaffDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingStaffDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
