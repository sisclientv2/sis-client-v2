import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CommonService} from './../../../../../core/services/common/common.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-teaching-staff-detail',
  templateUrl: './teaching-staff-detail.component.html',
  styleUrls: ['./teaching-staff-detail.component.css']
})
export class TeachingStaffDetailComponent implements OnInit {

  teachingStaffDetailData;

  constructor(private activatedRoute: ActivatedRoute, 
              private commonService: CommonService,
              private location: Location
              ) { }

  ngOnInit() {
    this.activatedRoute.params
      .map(params => params['userId'])
      .subscribe((userId) => {
        this.commonService.getUser(userId)
          .subscribe(teachingStaffDetail => {
            this.teachingStaffDetailData = teachingStaffDetail;
          })
      })
  }

  backPage(){
    this.location.back();
  }

}
