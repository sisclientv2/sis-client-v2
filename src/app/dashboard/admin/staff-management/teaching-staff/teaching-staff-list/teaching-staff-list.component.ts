import { Component, OnInit } from '@angular/core';
import {CommonService} from './../../../../../core/services/common/common.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-teaching-staff-list',
  templateUrl: './teaching-staff-list.component.html',
  styleUrls: ['./teaching-staff-list.component.css']
})
export class TeachingStaffListComponent implements OnInit {
  teachingStaffsData;
  searchValue;
  sortValue = 'asc';

  constructor(private commonService: CommonService, private router: Router) { }

  ngOnInit() {
    this.commonService.getAllUsers("teachingstaff")
      .subscribe(teachingStaff => {
        this.teachingStaffsData = teachingStaff;
      })
  }

  teachingStaffDetail(value){
    this.router.navigate(["/staffs/teachingStaff/teachingStaffDetail/", value]);
  }

  setSearchName(searchValue){
    this.searchValue = searchValue;
  }

  setSorting(sortValue){
    this.sortValue = sortValue;
  }

}
