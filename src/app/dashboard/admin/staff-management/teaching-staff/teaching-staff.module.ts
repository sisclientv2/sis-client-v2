import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TeachingStaffComponent} from './teaching-staff.component';
import { TeachingStaffCreateComponent } from './teaching-staff-create/teaching-staff-create.component';
import { TeachingStaffListComponent } from './teaching-staff-list/teaching-staff-list.component';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {DropdownModule} from 'primeng/primeng';
import {CalendarModule} from 'primeng/primeng';
import { TeachingStaffDetailComponent } from './teaching-staff-detail/teaching-staff-detail.component';
import {RouterModule} from "@angular/router";
import {SearchBarModule} from './../../shared/modules/search-bar/search-bar.module';
import { SortByPipe } from './pipes/sort-by.pipe';
import { SearchPipe } from './pipes/search.pipe';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    DropdownModule,
    CalendarModule,
    RouterModule,
    SearchBarModule
  ],
  declarations: [
    TeachingStaffComponent,
    TeachingStaffCreateComponent, 
    TeachingStaffListComponent, 
    TeachingStaffDetailComponent,
    SortByPipe,
    SearchPipe
  ]
})
export class TeachingStaffModule { }
