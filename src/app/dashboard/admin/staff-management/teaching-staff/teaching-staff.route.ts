import {Route} from '@angular/router';
import {TeachingStaffComponent} from './teaching-staff.component';
import {TeachingStaffListComponent} from './teaching-staff-list/teaching-staff-list.component';
import {TeachingStaffCreateComponent} from './teaching-staff-create/teaching-staff-create.component';
import {TeachingStaffDetailComponent} from './teaching-staff-detail/teaching-staff-detail.component';

export const TeachingStaffRoutes: Route[] = [
    {
        path: 'teachingStaff',
        component: TeachingStaffComponent,
        children: [
            {path: 'teachingStaffList', component: TeachingStaffListComponent},
            {path: 'teachingStaffCreate', component: TeachingStaffCreateComponent},
            {path: 'teachingStaffDetail/:userId', component: TeachingStaffDetailComponent}
        ]
    }
];