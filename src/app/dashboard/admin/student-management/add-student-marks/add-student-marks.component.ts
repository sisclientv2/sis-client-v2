import { Component, OnInit } from '@angular/core';
import { SubjectService } from './../../../../core/services/domain/subject.service';
import { StudentService } from './../../../../core/services/domain/student.service';
import { ExamService } from './../../../../core/services/domain/exam.service';
import { MarkService } from './../../../../core/services/domain/mark.service';
import { Subject } from './../../../../core/models/subject';
import { Mark } from './../../../../core/models/mark';
import { Exam } from './../../../../core/models/exam';
import { Car } from './../../../../core/models/car';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-add-student-marks',
    templateUrl: './add-student-marks.component.html',
    styleUrls: ['./add-student-marks.component.css']
})
export class AddStudentMarksComponent implements OnInit {

    cities;
    cars;
    marks: Mark;
    mark: Mark;
    student;
    exams;
    selectedExam;
    selectedSubject;
    exam: Exam;
    userId;


    displayDialog: boolean;

    subject: Subject;
    newMark: boolean;
    subjects;
    marklist=[];

    constructor(
        private subjectService: SubjectService,
        private studentService: StudentService,
        private activatedRoute: ActivatedRoute,
        private examService: ExamService,
        private markService: MarkService) {
    }

    ngOnInit() {
        this.getAllSubjects();
        this.activatedRoute.params
            .map(params => params['userId'])
            .subscribe((userId) => {
                this.studentService.getStudent(userId)
                    .subscribe(student => {
                        this.marks = student.marks;
                        this.userId = userId;
                    });
            });
        

    }

    getAllSubjects() {
        this.subjectService.getAllSubjects().subscribe(subjects => this.subjects = subjects);
    }


    showDialogToAdd() {
        this.newMark = true;
        this.mark = new Mark();
        let chkMark = this.mark.exam;
        let chkSubj = this.mark.subject;
        this.mark.studentName = this.userId;

        //   get Exams from service
        this.examService.getAllExams()
            .subscribe(exams => {
                this.exams = exams;
                this.exams.forEach(function (item) {
                    item.label = item.code;
                    chkMark = item;
                });
                console.log(chkMark);
                this.mark.exam = chkMark;
            });

        //   get Subjects from service
        this.subjectService.getAllSubjects()
            .subscribe(subjects => {
                this.subjects = subjects;
                this.subjects.forEach(function (item) {
                    item.label = item.code;
                    chkSubj = item;
                });
                this.mark.subject = chkSubj;
            });

        this.displayDialog = true;
    }

    save() {

        this.marklist.push(this.mark);

        if (this.newMark) {
            this.markService.createMarks(this.marklist).subscribe(() => {
                console.info("Marks Added Successfully");
                this.getAllSubjects();
            },
                error => {
                    console.info("Error Adding in Marks");
                });
        }

        this.mark = null;
        this.displayDialog = false;
    }

}
