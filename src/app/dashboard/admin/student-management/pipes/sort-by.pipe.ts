import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sortBy'
})
export class SortByPipe implements PipeTransform {

  transform(data: any[], args?: any): any[] {
    if(data){
       return data.sort((a: any, b: any) => {
            if (a.userId < b.userId) {
                return (args === "asc" ) ? -1 : 1;
            } else if (a.userId > b.userId) {
                return (args === "asc") ? 1 : -1;
            } else {
                return 0;
            }
        });
    }
    return data;
  }

}
