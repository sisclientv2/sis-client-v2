import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {StudentService} from '../../../../core/services/domain/student.service';
import {CommonService} from '../../../../core/services/common/common.service';
// import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.css']
})
export class StudentCreateComponent implements OnInit {


    genders;
    grades;
    userTypes;
  
  private createStudentForm: FormGroup;

  constructor(private fb: FormBuilder, private studentService: StudentService, private commonService: CommonService) {

  }

  ngOnInit() {

    // get common data from common service
    this.userTypes = this.commonService.getUserTypes();
    this.genders = this.commonService.getGenders();
    this.grades = this.commonService.getGrades();



    this.createStudentForm = this.fb.group({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl(),
      userId: new FormControl(),
      userType: new FormControl(),
      birthDate: new FormControl(),
      address: new FormGroup({
        address1: new FormControl(),
        address2: new FormControl(),
        city: new FormControl(),
        state: new FormControl(),
        pin: new FormControl(),
        country: new FormControl()
      }),
      gender: new FormControl(),
      image: new FormControl(),
      grade: new FormControl()

    });
  }

  saveStudentData(value){
      this.studentService.createStudents(value).subscribe(() => {
          console.info("Student Added Successfully");
          this.createStudentForm.reset();
      },
      error => {
        console.info("Error Adding in Student Data");
      });
  }

}
