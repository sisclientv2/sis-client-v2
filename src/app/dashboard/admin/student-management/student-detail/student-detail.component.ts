import { Component, OnInit } from '@angular/core';
import {StudentService} from './../../../../core/services/domain/student.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.css']
})
export class StudentDetailComponent implements OnInit {

  userId;
  id;
  studentData = {
    address:{}
  };

  constructor(private studentService: StudentService, 
              private route: ActivatedRoute,
              private _location: Location
               ) {
  }

  ngOnInit() {
      this.route.params
      .map(params => params['userId'])
      .subscribe((userId) => {
          this.studentService.getStudent(userId)
            .subscribe(student => {
                this.studentData = student;
            })
      })
  }


  backPage(){
    this._location.back();
  }

}
