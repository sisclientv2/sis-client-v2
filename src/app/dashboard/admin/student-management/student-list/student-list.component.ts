import { Component, OnInit } from '@angular/core';
import {StudentService} from '../../../../core/services/domain/student.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
    students;
    user1;
    searchValue;
    sortValue = 'asc';

  constructor(private _studentService: StudentService,
              private router: Router
  ) { }

  ngOnInit() {
    /*
     * Load the students from service
     */
    this._studentService.getAllStudents()
      .subscribe(students => {
        this.students = students;
      });

  }
  
  viewStudentDetail(userId){
    this.router.navigate(['/students/studentsDetail/', userId]);
  }

  setSearchName(searchValue){
    this.searchValue = searchValue;
  }

  setSorting(sortValue){
    this.sortValue = sortValue;
  }

  addMarks(userId){
    this.router.navigate(['/students/addStudentMarks/', userId]);
  }



}
