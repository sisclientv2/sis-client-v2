import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {StudentManagementComponent} from './student-management.component';
import {StudentListComponent} from './student-list/student-list.component';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import { StudentCreateComponent } from './student-create/student-create.component';
import {CalendarModule} from 'primeng/primeng';
import {DropdownModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/primeng';
import { StudentDetailComponent } from './student-detail/student-detail.component';
import {SearchBarModule} from './../shared/modules/search-bar/search-bar.module';
import { SortByPipe } from './pipes/sort-by.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { AddStudentMarksComponent } from './add-student-marks/add-student-marks.component';
import {InputTextModule,DataTableModule,ButtonModule,DialogModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    DropdownModule,
    FileUploadModule,
    InputTextModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    SearchBarModule
    
  ],
  exports: [
    StudentManagementComponent
  ],
  declarations: [
    StudentManagementComponent,
    StudentListComponent,
    StudentCreateComponent,
    StudentDetailComponent,
    SortByPipe,
    SearchPipe,
    AddStudentMarksComponent
  ]
})
export class StudentManagementModule { }
