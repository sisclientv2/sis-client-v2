import {Route} from '@angular/router';
import {StudentManagementComponent} from './student-management.component';
import {StudentListComponent} from './student-list/student-list.component';
import {StudentCreateComponent} from './student-create/student-create.component';
import {StudentDetailComponent} from './student-detail/student-detail.component';
import {AddStudentMarksComponent} from './add-student-marks/add-student-marks.component';

export const StudentManagementRoutes: Route[] = [
    {
        path: 'students',
        component: StudentManagementComponent,
        children: [
            {path: 'studentsList', component: StudentListComponent},
            {path: 'studentsCreate', component: StudentCreateComponent},
            {path: 'studentsDetail/:userId', component: StudentDetailComponent},
            {path: 'addStudentMarks/:userId', component: AddStudentMarksComponent}
        ]
    }
];