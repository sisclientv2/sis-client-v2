import { Component, OnInit } from '@angular/core';
import {SubjectService} from './../../../core/services/domain/subject.service';
import {Subject} from './../../../core/models/subject';

@Component({
  selector: 'app-subject-management',
  templateUrl: './subject-management.component.html',
  styleUrls: ['./subject-management.component.css']
})
export class SubjectManagementComponent implements OnInit {

    displayDialog: boolean;

    subject: Subject;
    newSubject: boolean;
    subjects: Subject;

  constructor(private subjectService: SubjectService) { }

  ngOnInit() {
      this.getAllSubjects();
  }

  getAllSubjects(){
      this.subjectService.getAllSubjects().subscribe(subjects => this.subjects = subjects);
  }


  showDialogToAdd() {
        this.newSubject = true;
        this.subject = new Subject();
        this.displayDialog = true;
    }

    save() {
        if(this.newSubject){
            this.subjectService.createSubject(this.subject).subscribe(() => {
                console.info("Subject Added Successfully");
                this.getAllSubjects();
            },
            error => {
                console.info("Error Adding in Subject Data");
            });
        }else if(!this.newSubject){
            this.subjectService.updateSubject(this.subject).subscribe(() => {
                console.info("Subject Updated Successfully");
                this.getAllSubjects();
            },
            error => {
                console.info("Error Updating in Subject Data");
            });
        }

        this.subject = null;
        this.displayDialog = false;
    }

    delete() {
        this.subjectService.deleteSubject(this.subject.code).subscribe(() => {
            console.info("Subject Deleted Successfully");
            this.getAllSubjects();
        },
        error => {
            console.info("Error Deleting in Subject Data");
        });

        this.subject = null;
        this.displayDialog = false;
    }

    onRowSelect(event) {
        this.newSubject = false;
        this.subject = this.cloneSubject(event.data);
        this.displayDialog = true;
    }

    cloneSubject(c: Subject): Subject {
        let subject = new Subject();
        for(let prop in c) {
            subject[prop] = c[prop];
        }
        return subject;
    }

}
