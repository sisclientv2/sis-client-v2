import { NgModule } from '@angular/core';
import { SubjectManagementComponent } from './subject-management.component';
import { CommonModule } from '@angular/common';
import {InputTextModule,DataTableModule,ButtonModule,DialogModule} from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    InputTextModule,
    DataTableModule,
    ButtonModule,
    DialogModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    SubjectManagementComponent
  ]
})
export class SubjectManagementModule { }
