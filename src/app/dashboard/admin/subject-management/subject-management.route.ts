import {Route} from '@angular/router';
import {SubjectManagementComponent} from './subject-management.component';

export const SubjectManagementRoutes: Route[] = [
    {
        path: 'subjects',
        component: SubjectManagementComponent
    }
];