import { Component, OnInit } from '@angular/core';
import { DummyDataService } from './../../../core/services/common/dummy-data.service';
import { Test } from './../../../core/models/test';
import { Exam } from './../../../core/models/exam';
import { SelectItem } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { ExamService } from './../../../core/services/domain/exam.service';

@Component({
  selector: 'app-test-management',
  templateUrl: './test-management.component.html',
  styleUrls: ['./test-management.component.css']
})
export class TestManagementComponent implements OnInit {
  exam: SelectItem[];

  exams;
  selectedExam;



  constructor(private dummyDataService: DummyDataService, private examService: ExamService) {

  }

  ngOnInit() {
    // get exams from service
    this.examService.getAllExams()
      .subscribe(exams => {
        this.exams = exams;

        // assign label and value property to new exam object
        let y = [{ value: null, label: 'Select Exam' }];
        this.exams.forEach(function (item) {
          var x = { value: {}, label: '' };
          x.value = item;
          x.label = item.code;
          y.push(x);
        });
        this.exam = y;

      });


  }

  addExam() {
    if (this.selectedExam) {
      let div = document.createElement("div");
      let h4 = document.createElement("h4");
      div.className = 'exam-div';
      div.appendChild(h4);
      h4.innerHTML = this.selectedExam.code;
      let ediv = document.getElementById('exam-cont');
      ediv.appendChild(div);
    }

  }


}
