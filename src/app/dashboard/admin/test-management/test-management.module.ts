import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TestManagementComponent} from './test-management.component';
import { BrowserModule } from '@angular/platform-browser';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {DropdownModule} from 'primeng/primeng';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule
    
  ],
  declarations: [TestManagementComponent]
})
export class TestManagementModule { }
