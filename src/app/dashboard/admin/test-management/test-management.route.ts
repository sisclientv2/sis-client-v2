import {Route} from '@angular/router';
import {TestManagementComponent} from './test-management.component';

export const TestManagementRoutes: Route[] = [
    {
        path: 'test',
        component: TestManagementComponent
    }
];