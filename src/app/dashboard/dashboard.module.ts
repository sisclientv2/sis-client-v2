import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {AdminModule} from './admin/admin.module';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './../dashboard/navbar/navbar.component';

@NgModule({
  imports: [
    CommonModule,
    AdminModule,
    RouterModule
  ],
  declarations: [
    DashboardComponent,
    NavbarComponent
  ],
  exports: [DashboardComponent, NavbarComponent]
})
export class DashboardModule { }
